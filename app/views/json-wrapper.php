<?php
$success = isset($success) ? json_encode($success) : json_encode(false);
$status = isset($status) ? json_encode($status) : json_encode(500);
$messages = isset($messages) ? json_encode($messages) : json_encode(array());
$data = isset($data) ? json_encode($data) : json_encode(new stdClass());
?>
{
	"success" : <?php echo $success; ?>,
	"status" : <?php echo $status; ?>,
	"messages" : <?php echo $messages; ?>,
	"data" : <?php echo $data; ?>
}