<?php
class WeekendsModel {
	/**
	 * This function returns the weekends, a size can be passed to limit the amount returned
	 **/
	public static function get($size = false) {
		// Our initial query
		$query = DB::table('weekends')->where( 'saturday', '>=', DB::raw('CURDATE()') );

		// Whether or not to add a limit and return the result
		if ($size === false)
			return $query->get();
		else
			return $query->take($size)->get();
	}
}