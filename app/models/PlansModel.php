<?php
class PlansModel {
	/**
	 * This function adds the rawPlan to the DB and returns success
	 **/
	public static function add($rawPlan) {
		// Standardize the plan
		$plan = self::digestPlan($rawPlan);

		// Creating Master, but first we check if there's already a person
		$id = false;
		$person = DB::table('people')->select('id')->where('email', '=', $plan->person->email)->first();
		if (!empty($person))
			$id = $person->id;

		// If it's a new person, we create them and get their ID
		if ($id === false) {
			$id = DB::table('people')->insertGetId(array(
				'created' => DB::raw('NOW()'),
				'name' => $plan->person->name,
				'email' => $plan->person->email,
			));
		}

		// Adding the plan in
		return DB::table('plans')->insert(array(
			'created' => DB::raw('NOW()'),
			'name' => $plan->plan->name,
			'first_weekend_id' => $plan->plan->first_weekend_id,
			'second_weekend_id' => $plan->plan->second_weekend_id,
			'master_person_id' => $id,
		));
	}

	/**
	 * Returns $size number of plans, if no $size is provided, supplies all of them
	 **/
	public static function get($size = false) {
		$query = DB::table('plans')->
			leftJoin('people', 'plans.master_person_id', '=', 'people.id')->
			leftJoin('weekends as firstWeekend', 'plans.first_weekend_id', '=', 'firstWeekend.id')->
			leftJoin('weekends as secondWeekend', 'plans.second_weekend_id', '=', 'secondWeekend.id')->
			select('plans.*', 'people.name as owner_name', 'firstWeekend.saturday as start', 'secondWeekend.saturday as end')->
			where('plans.active', '=', 'yes');

		if ($size === false)
			$plans = $query->get();
		else
			$plans = $query->take($size)->get();

		return $plans;
	}

	/**
	 * Turns an AJAXed new plan into a PHP object
	 **/
	private static function digestPlan($rawPlan) {
		$out = new stdClass();
		$out->person = new stdClass();
		$out->plan = new stdClass();

		$out->person->name = isset($rawPlan['ownerName']) ? $rawPlan['ownerName'] : DB::raw('NULL');
		$out->person->email = isset($rawPlan['ownerEmail']) ? $rawPlan['ownerEmail'] : 'Unknown';

		$out->plan->name = isset($rawPlan['name']) ? $rawPlan['name'] : 'Unknown';
		$out->plan->first_weekend_id = isset($rawPlan['start']['id']) ? $rawPlan['start']['id'] : 0;
		$out->plan->second_weekend_id = isset($rawPlan['end']['id']) ? $rawPlan['end']['id'] : 0;

		return $out;
	}
}