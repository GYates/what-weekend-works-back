<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::get('/weekends', function() {
	$data = new stdClass();
	$data->weekends = WeekendsModel::get();

	$info = array(
		'success' => true,
		'status' => 200,
		'data' => $data
	);

	return View::make('json-wrapper', $info);
});

Route::get('/plans/{limit?}', function($limit = false) {
	$plans = PlansModel::get($limit);

	if ($plans !== false) {
		$status = 200;
		$info = array(
			'success' => true,
			'status' => $status,
			'data' => $plans
		);
	} else {
		$status = 503;
		$info = array(
			'success' => false,
			'status' => $status
		);
	}

	return Response::view('json-wrapper', $info, $status);
});

Route::post('/plans', function() {
	$addSuccess = PlansModel::add(Input::all());

	if ($addSuccess) {
		$status = 204;
		$info = array(
			'success' => true,
			'status' => $status,
		);
	} else {
		$status = 503;
		$info = array(
			'success' => false,
			'status' => $status,
		);
	}

	return Response::view('json-wrapper', $info, $status);
});