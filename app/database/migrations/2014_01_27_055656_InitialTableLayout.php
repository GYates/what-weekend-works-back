<?php

use Illuminate\Database\Migrations\Migration;

class InitialTableLayout extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plans', function($table) {
			$table->increments('id')->unsigned();
			$table->dateTime('created');
			$table->string('name', 100);
			$table->integer('first_weekend_id')->unsigned();
			$table->integer('second_weekend_id')->unsigned();
			$table->integer('master_person_id')->unsigned();
		});

		Schema::create('people_plans', function($table) {
			$table->increments('id')->unsigned();
			$table->dateTime('created');			
			$table->integer('plans_id')->unsigned();
			$table->integer('people_id')->unsigned();
		});

		Schema::create('weekends', function($table) {
			$table->increments('id')->unsigned();
			$table->date('saturday');

			$table->unique('saturday');
		});

		Schema::create('available_weekends', function($table) {
			$table->integer('people_plans_id')->unsigned();
			$table->integer('weekends_id')->unsigned();
			$table->dateTime('created');
			$table->dateTime('updated');
			$table->enum('available_saturday', array('yes', 'no'));
			$table->enum('available_sunday', array('yes', 'no'));

			$table->primary(array('people_plans_id', 'weekends_id'));
		});

		Schema::create('people', function($table) {
			$table->increments('id')->unsigned();
			$table->dateTime('created');
			$table->string('name', 100)->nullable();
			$table->string('email', 255);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plans');
		Schema::drop('people_plans');
		Schema::drop('weekends');
		Schema::drop('available_weekends');
		Schema::drop('people');
	}

}