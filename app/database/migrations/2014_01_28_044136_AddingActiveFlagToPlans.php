<?php

use Illuminate\Database\Migrations\Migration;

class AddingActiveFlagToPlans extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('plans', function($table) {
			$table->enum('active', array('yes', 'no'))->after('created');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('plans', function($table) { $table->dropColumn('active'); });
	}

}