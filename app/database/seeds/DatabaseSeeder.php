<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('WeekendsTableSeeder');
	}

}

class WeekendsTableSeeder extends Seeder {
	/**
	 * This function iterates through $years worth of saturdays and
	 * creates those columns in the weekends table
	 **/
	public function run() {
		$time = strtotime('Saturday'); // Getting the UNIX timestamp of the next weekend
		$seconds_in_a_week = 60*60*24*7; // Generating a constant
		$years = 10; // The number of years worth of weekends to create

		$weekendsArr = array();

		// Here we build the array of rows
		for ($i = 0; $i < 52*$years; $i++) {
			$date = date( 'Y-m-d', $time+($seconds_in_a_week*$i) );

			$weekendsArr[] = array(
				'saturday' => $date
			);
		}

		// Insert the rows into the table
		DB::table('weekends')->insert($weekendsArr);
	}
}